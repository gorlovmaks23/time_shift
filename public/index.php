<?php

use Phalcon\DI\FactoryDefault;
use Phalcon\Mvc\Application;
use Timeshift\Models\Settings;

error_reporting(E_ALL);

/**
 * Define some useful constants
 */
define('BASE_PATH', dirname(__DIR__));
define('APP_PATH', BASE_PATH . '/app');

try {

    /**
     * The FactoryDefault Dependency Injector automatically register the right services providing a full stack framework
     */
    $di = new FactoryDefault();

    /**
     * Read services
     */
    include APP_PATH . "/config/services.php";

    /**
     * Get config service for use in inline setup below
     */
    $config = $di->getConfig();

    /**
     * Include Autoloader
     */
    include APP_PATH . '/config/loader.php';

    $settings = $di->getSettings();


    foreach ($settings as $setting) {
        if (!Settings::findFirst([
            'conditions' => 'name = :name:',
            'bind' => [
                'name' => $setting['name'],
            ],
        ])) {
            (new Settings([
                'name' => $setting['name'],
                'value' => $setting['defaultValue']
            ]))->save();
        }
    }

    /**
     * Handle the request
     */
    $application = new Application($di);

    echo $application->handle()
        ->getContent();

} catch (Exception $e) {
    echo $e->getMessage(), '<br>';
    echo nl2br(htmlentities($e->getTraceAsString()));
}
