<?php

use Phalcon\Config;
use Timeshift\Lists\SettingsKeys;

return new Config([
    SettingsKeys::START_OF_THE_WORKING_DAY => [
        'name' => SettingsKeys::START_OF_THE_WORKING_DAY,
        'defaultValue' => '09:00'
    ],
    SettingsKeys::END_OF_THE_WORKING_DAY => [
        'name' => SettingsKeys::END_OF_THE_WORKING_DAY,
        'defaultValue' => '18:00'
    ]
]);
