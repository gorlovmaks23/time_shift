<?php

use Phalcon\Config;

return new Config([
    'privateResources' => [
        'admin' => [
            'index',
        ],
        'users' => [
            'index',
            'search',
            'edit',
            'create',
            'search',
            'delete',
            'changePassword',
        ],
        'tracker' => [
            'start',
            'stop',
        ],
        'profiles' => [
            'index',
            'create',
            'search',
            'edit',
            'delete'
        ],
        'celebrations' => [
            'index',
            'create',
            'search',
            'edit',
            'delete'
        ],
        'settings' => [
            'index',
            'create',
            'search',
            'edit',
            'delete'
        ],
        'index' => [
            'index'
        ],
        'profile' => [
            'changePassword'
        ],
    ],
]);
