<?php

$router->add(
    '/admin/celebrations',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'celebrations',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/celebrations/create',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'celebrations',
        'action'     => 'create',
    ]
);

$router->add(
    '/admin/celebrations/edit/{id}',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'celebrations',
        'action'     => 'edit',
    ]
);

$router->add(
    '/admin/celebrations/delete/{id}',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'celebrations',
        'action'     => 'delete',
    ]
);

$router->add(
    '/admin/celebrations/search',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'celebrations',
        'action'     => 'search',
    ]
);
