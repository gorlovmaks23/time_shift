<?php

$router->add(
    '/admin',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'admin',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/auth',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'session',
        'action'     => 'auth',
    ]
);

$router->add(
    '/admin/logout',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'session',
        'action'     => 'logout',
    ]
);
