<?php


$router->add(
    '/admin/permissions',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'permissions',
        'action'     => 'index',
    ]
);
