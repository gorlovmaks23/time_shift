<?php

$router->add(
    '/admin/users',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/users/create',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'create',
    ]
);

$router->add(
    '/admin/users/edit/{id}',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'edit',
    ]
);

$router->add(
    '/admin/users/search',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'search',
    ]
);

$router->add(
    '/admin/users/changePassword',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'changePassword',
    ]
);

$router->add(
    '/admin/users/delete/{id}',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'users',
        'action'     => 'delete',
    ]
);
