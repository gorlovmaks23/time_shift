<?php

$router->add(
    '/admin/settings',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'settings',
        'action'     => 'index',
    ]
);

$router->add(
    '/admin/settings/create',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'settings',
        'action'     => 'create',
    ]
);

$router->add(
    '/admin/settings/edit/{id}',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'settings',
        'action'     => 'edit',
    ]
);

$router->add(
    '/admin/settings/search',
    [
        'namespace'  => 'Timeshift\Controllers\Admin',
        'controller' => 'settings',
        'action'     => 'search',
    ]
);
