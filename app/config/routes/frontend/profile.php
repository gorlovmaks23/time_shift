<?php

$router->add(
    '/profile/changePassword',
    [
        'namespace'  => 'Timeshift\Controllers',
        'controller' => 'profile',
        'action'     => 'changePassword',
    ]
);
