<?php

$router->add(
    '/celebration',
    [
        'namespace'  => 'Timeshift\Controllers',
        'controller' => 'celebration',
        'action'     => 'index',
    ]
);
