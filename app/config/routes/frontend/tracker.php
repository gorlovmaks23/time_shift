<?php

$router->add(
    '/start',
    [
        'namespace'  => 'Timeshift\Controllers',
        'controller' => 'tracker',
        'action'     => 'start',
    ]
);

$router->add(
    '/stop',
    [
        'namespace'  => 'Timeshift\Controllers',
        'controller' => 'tracker',
        'action'     => 'stop',
    ]
);
