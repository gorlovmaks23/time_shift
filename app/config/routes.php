<?php

$router = new Phalcon\Mvc\Router();

include_once APP_PATH.'/config/routes/admin.php';
include_once APP_PATH.'/config/routes/frontend.php';

$router->add('/confirm/{code}/{email}', [
    'controller' => 'user_control',
    'action' => 'confirmEmail'
]);

$router->add('/reset-password/{code}/{email}', [
    'controller' => 'user_control',
    'action' => 'resetPassword'
]);

return $router;
