<?php

use Phalcon\Loader;

$loader = new Loader();

/**
 * We're a registering a set of directories taken from the configuration file
 */
$loader->registerNamespaces([
    //Default namespaces
    'Timeshift\Models'      => $config->application->modelsDir,
    'Timeshift\Controllers' => $config->application->controllersDir,
    'Timeshift\Forms'       => $config->application->formsDir,
    'Timeshift'             => $config->application->libraryDir,
    'Timeshift\Controllers\Admin' => $config->application->adminControllerDir,
    'Timeshift\Repository' => $config->application->repositoryDir,
    'Timeshift\Services' => $config->application->servicesDir,
    'Timeshift\Lists' => $config->application->listsDir,
]);

$loader->register();

// Use composer autoloader to load vendor classes
require_once BASE_PATH . '/vendor/autoload.php';
