<?php

declare(strict_types=1);

namespace Timeshift\Repository;

use Timeshift\Models\Settings;

final class SettingsRepository
{
    public function findFirstByName(string $name)
    {
        return Settings::findFirst(
            [
                'conditions' => 'name = :name:',
                'bind' => [
                    'name' => $name,
                ],
            ]
        );
    }
}
