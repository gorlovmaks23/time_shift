<?php

declare(strict_types=1);

namespace Timeshift\Repository;

use Timeshift\Models\Tracker;

final class TrackerRepository
{
    public function lastTrackByUserId(string $userId)
    {
        return Tracker::findFirst([
            'conditions' => 'userId = :userId:',
            'bind' => [
                'userId' => $userId,
            ],
            'order' => 'touchDate DESC',
        ]);
    }
}
