<?php

declare(strict_types=1);

namespace Timeshift\Repository;

use Phalcon\Mvc\Model\ResultsetInterface;
use Timeshift\Models\Users;

final class UserRepository
{
    public function findAll(array $criteria): ResultsetInterface
    {
        return Users::find($criteria);
    }

    public function findById(int $id): Users
    {
        return Users::findFirstById($id);
    }
}
