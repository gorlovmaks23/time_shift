<?php

declare(strict_types=1);

namespace Timeshift\Repository;

use Carbon\Carbon;
use Timeshift\Models\Celebrations;

final class CelebrationRepository
{
    public function celebrationForCurrentDay()
    {
        return Celebrations::findFirst([
            'conditions' => 'date in (:oneTime:, :repetitive:)',
            'bind' => [
                'oneTime' => Carbon::now()->format('Y-m-d'),
                'repetitive' => Carbon::now()->format('m-d'),
            ],
        ]);
    }
}
