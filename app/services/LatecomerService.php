<?php

declare(strict_types=1);

namespace Timeshift\Services;

use Carbon\Carbon;
use Timeshift\Models\Latecomers;

final class LatecomerService
{
    /** TODO Need to make the use of the service more convenient  */
    public function write(int $userId)
    {
        $latercomer = new Latecomers([
            'date' => Carbon::now()->format('Y-m-d'),
            'userId' => $userId,
        ]);

        if(!$latercomer->save())
        {
            throw new Exception();
        }
    }
}
