<?php

declare(strict_types=1);

namespace Timeshift\Services;

use Timeshift\Models\Settings;
use Timeshift\Repository\SettingsRepository;

final class SettingService
{
    private $settingsRepository;

    public function __construct(
        SettingsRepository $settingsRepository
    )
    {
        $this->settingsRepository = $settingsRepository;
    }

    public function get(string $name): ?string
    {
        /** @var Settings $setting */
        $setting = $this->settingsRepository->findFirstByName($name);

        if (!$setting) {
            return null;
        }

        return $setting->value;
    }
}
