<?php

declare(strict_types=1);

namespace Timeshift\Lists;

final class SettingsKeys
{
    public const START_OF_THE_WORKING_DAY = 'start_of_the_working_day';
    public const END_OF_THE_WORKING_DAY = 'end_of_the_working_day';
}
