<?php

declare(strict_types=1);

namespace Timeshift\Forms;

use Phalcon\Forms\Element\Date;
use Phalcon\Forms\Element\Hidden;
use Phalcon\Forms\Element\Select;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;
use Phalcon\Validation\Validator\PresenceOf;

final class CelebrationsForm extends Form
{
    public function initialize($entity = null, $options = null)
    {
        // In edition the id is hidden
        if (isset($options['edit']) && $options['edit']) {
            $id = new Hidden('id');
        } else {
            $id = new Text('id');
        }

        $this->add($id);

        $name = new Text('name');
        $name->setLabel('Name');

        $name->addValidators([
            new PresenceOf([
                'message' => 'The name is required'
            ])
        ]);

        $this->add($name);

        $date = new Date('date');
        $date->setLabel('Date');

        $this->add($date);

        $repetitive = new Select('repetitive', [
            'Y' => 'Yes',
            'N' => 'No'
        ]);

        $repetitive->setLabel('Repetitive');
        $this->add($repetitive);
    }
}
