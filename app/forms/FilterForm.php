<?php

declare(strict_types=1);

namespace Timeshift\Forms;

use Phalcon\Forms\Element\Submit;
use Phalcon\Forms\Element\Text;
use Phalcon\Forms\Form;

final class FilterForm extends Form
{
    public function initialize()
    {
        $date = new Text('date');

        $this->add($date);

        $this->add(new Submit('Send'));
    }
}
