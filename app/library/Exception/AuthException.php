<?php

declare(strict_types=1);

namespace Timeshift\Exception;

final class AuthException extends \Exception
{
    public static function wrongCombination(): self
    {
        return new self('Wrong email/password combination');
    }

    public static function banned(): self
    {
        return new self('The user is banned');
    }

    public static function inactive(): self
    {
        return new self('The user is inactive');
    }

    public static function suspended(): self
    {
        return new self('The user is suspended');
    }

    public static function userNotExist(): self
    {
        return new self('The user does not exist');
    }
}