<?php

declare(strict_types=1);

namespace Timeshift\Controllers\Admin;

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Timeshift\Forms\CelebrationsForm;
use Timeshift\Models\Celebrations;

final class CelebrationsController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->view->setTemplateBefore('celebrations/index');
    }

    public function indexAction()
    {
        $this->persistent->conditions = null;
        $this->view->form = new CelebrationsForm();
    }

    public function createAction()
    {
        if ($this->request->isPost()) {
            $celebration = new Celebrations([
                'name' => $this->request->getPost('name'),
                'date' => $this->request->getPost('date'),
                'repetitive' => $this->request->getPost('repetitive')
            ]);

            if (!$celebration->save()) {
                $this->flash->error($celebration->getMessages());
            } else {
                $this->flash->success("Celebration was created successfully");
            }
        }

        $this->view->setTemplateBefore('celebrations/create');
        $this->view->form = new CelebrationsForm(null);
        $this->view->form->clear();
    }

    /**
     * Searches for celebrations
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, Celebrations::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $celebrations = Celebrations::find($parameters);
        if (count($celebrations) == 0) {
            $this->flash->notice("The search did not find any celebrations");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $celebrations,
            "limit" => 10,
            "page" => $numberPage
        ]);

        $this->view->setTemplateBefore('celebrations/search');
        $this->view->page = $paginator->getPaginate();
    }

    public function editAction($id)
    {
        $celebration = Celebrations::findFirstById($id);

        if (!$celebration) {
            $this->flash->error("Celebration was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if ($this->request->isPost()) {
            $date = $this->request->getPost('date');

            if ($this->request->getPost('repetitive') === 'Y') {
                $date = date('m-d', strtotime($date));
            }

            $celebration->assign([
                'name' => $this->request->getPost('name'),
                'date' => $date,
                'repetitive' => $this->request->getPost('repetitive')
            ]);

            if (!$celebration->save()) {
                $this->flash->error($celebration->getMessages());
            } else {
                $this->flash->success("Profile was updated successfully");
            }
        }

        $this->view->setTemplateBefore('celebrations/edit');

        $this->view->form = new CelebrationsForm($celebration, [
            'edit' => true
        ]);
        $this->view->form->clear();

        $this->view->celebration = $celebration;
    }

    public function deleteAction($id)
    {
        $profile = Celebrations::findFirstById($id);
        if (!$profile) {
            $this->flash->error("Celebration was not found");

            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }

        if (!$profile->delete()) {
            $this->flash->error($profile->getMessages());
        } else {
            $this->flash->success("Celebration was deleted");
        }

        return $this->dispatcher->forward([
            'action' => 'index'
        ]);
    }
}
