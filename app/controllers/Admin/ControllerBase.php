<?php

declare(strict_types=1);

namespace Timeshift\Controllers\Admin;

use Phalcon\Mvc\Controller;
use Phalcon\Mvc\Dispatcher;

abstract class ControllerBase extends Controller
{
    public function initialize()
    {
        $this->view->setLayoutsDir('admin/');
        $this->view->setLayout('layouts/private');
    }

    public function beforeExecuteRoute(Dispatcher $dispatcher)
    {
        $controllerName = $dispatcher->getControllerName();

        // Only check permissions on private controllers
        if ($this->acl->isPrivate($controllerName)) {
            // Get the current identity
            $identity = $this->auth->getIdentity();
            // If there is no identity available the user is redirected to index/index
            if (!is_array($identity)) {
                $this->flash->notice('You don\'t have access to this module: private');

                $dispatcher->forward([
                    'controller' => 'session',
                    'action' => 'auth'
                ]);

                return false;
            }

            // Check if the user have permission to the current option
            $actionName = $dispatcher->getActionName();

//            dd($this->acl->isAllowed($identity['profile'], $controllerName, $actionName));
            if (!$this->acl->isAllowed($identity['profile'], $controllerName, $actionName)) {

                $this->flash->notice('You don\'t have access to this module: ' . $controllerName . ':' . $actionName);

                if ($this->acl->isAllowed($identity['profile'], $controllerName, 'index')) {

                    $this->view->setLayoutsDir('admin/');
                    $this->view->setLayout('layouts/private');

                    $dispatcher->forward([
                        'controller' => $controllerName,
                        'action' => 'index'
                    ]);
                } else {
                    $dispatcher->forward([
                        'controller' => 'session',
                        'action' => 'auth'
                    ]);
                }

                return false;
            }
        }
    }
}