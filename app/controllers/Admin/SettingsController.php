<?php

declare(strict_types=1);

namespace Timeshift\Controllers\Admin;

use Phalcon\Mvc\Model\Criteria;
use Phalcon\Paginator\Adapter\Model as Paginator;
use Timeshift\Forms\SettingsForm;
use Timeshift\Models\Settings;

final class SettingsController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();
        $this->view->setTemplateBefore('settings/index');
    }

    public function indexAction()
    {
        $this->persistent->conditions = null;
        $this->view->form = new SettingsForm();
    }

    public function createAction()
    {
        if ($this->request->isPost()) {

            $setting = new Settings([
                'name' => $this->request->getPost('name'),
                'value' => $this->request->getPost('value'),
            ]);

            if (!$setting->save()) {
                $this->flash->error($setting->getMessages());
            } else {
                $this->flash->success("Setting was created successfully");
            }
        }

        $this->view->setTemplateBefore('settings/create');
        $this->view->form = new SettingsForm(null);
        $this->view->form->clear();
    }

    /**
     * Searches for $settings
     */
    public function searchAction()
    {
        $numberPage = 1;
        if ($this->request->isPost()) {
            $query = Criteria::fromInput($this->di, Settings::class, $this->request->getPost());
            $this->persistent->searchParams = $query->getParams();
        } else {
            $numberPage = $this->request->getQuery("page", "int");
        }

        $parameters = [];
        if ($this->persistent->searchParams) {
            $parameters = $this->persistent->searchParams;
        }

        $settings = Settings::find($parameters);
        if (count($settings) == 0) {
            $this->flash->notice("The search did not find any settings");

            return $this->dispatcher->forward([
                "action" => "index"
            ]);
        }

        $paginator = new Paginator([
            "data" => $settings,
            "limit" => 10,
            "page" => $numberPage
        ]);

        $this->view->setTemplateBefore('settings/search');
        $this->view->page = $paginator->getPaginate();
    }

    public function editAction($id)
    {
        $setting = Settings::findFirstById($id);
        if (!$setting) {
            $this->flash->error("Setting was not found");
            return $this->dispatcher->forward([
                'action' => 'index'
            ]);
        }
        if ($this->request->isPost()) {
            $setting->assign([
                'name' => $this->request->getPost('name'),
                'value' => $this->request->getPost('value'),
            ]);

            if (!$setting->save()) {
                $this->flash->error($setting->getMessages());
            } else {
                $this->flash->success("Profile was updated successfully");
            }
        }

        $this->view->setTemplateBefore('settings/edit');

        $this->view->form = new SettingsForm($setting, [
            'edit' => true
        ]);
        $this->view->form->clear();

        $this->view->setting = $setting;
    }
}
