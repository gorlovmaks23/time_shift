<?php

declare(strict_types=1);

namespace Timeshift\Controllers;

use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Phalcon\Mvc\Model\ResultsetInterface;
use Timeshift\Forms\FilterForm;
use Timeshift\Models\Tracker;
use Timeshift\Repository\TrackerRepository;
use Timeshift\Repository\UserRepository;

final class IndexController extends ControllerBase
{
    /** @var UserRepository $userRepository */
    private $userRepository;

    /** @var TrackerRepository $trackRepository */
    private $trackRepository;

    /** @var array $totalDurationOfWork */
    private $totalDurationOfWork;

    public function initialize()
    {
        $this->userRepository = $this->di->get(UserRepository::class);
        $this->trackRepository = $this->di->get(TrackerRepository::class);
        $this->totalDurationOfWork = [];
    }

    public function indexAction()
    {
        $currentUser = $this->auth->getUser();
        $currentDay = Carbon::now();

        $filterForm = new FilterForm();
        $filter = $this->request->get('date') ?? null;

        $datePeriod = $this->periodOfMonth($filter);
        $lastTrack = $this->trackRepository->lastTrackByUserId($this->auth->getIdentity()['id']);

        $users = $this->userRepository->findAll(
            [
                'order' => 'CASE id WHEN ' . $this->auth->getIdentity()['id'] . ' THEN 1 END DESC',
            ]
        );

        $this->calculateTotalTime($users);
        $this->view->setVar('logged_in', is_array($this->auth->getIdentity()));
        $this->view->setVar('users', $users);
        $this->view->setVar('lastTrack', $lastTrack);
        $this->view->setVar('startType', Tracker::START);
        $this->view->setVar('currentUser', $currentUser);
        $this->view->setVar('datePeriod', $datePeriod);
        $this->view->setVar('totalDurationOfWork', $this->totalDurationOfWork);
        $this->view->setVar('currentDay', $currentDay);
        $tracker = $this->view->getPartial("tracker");
        $this->view->setVar('tracker', $tracker);
        $this->view->form = $filterForm;
        $this->view->setTemplateBefore('public');
    }

    private function calculateTotalTime(ResultsetInterface $users): array
    {
        foreach ($users as $user) {
            for ($i = 0; $i < count($user->tracks); $i++) {
                $count = $i + 1;
                $track = $user->tracks[$i];

                $nextTrack = isset($user->tracks[$count]) ? $user->tracks[$count] : null;
                $dateIndex = date('Y-m-d', (int)$track->touchDate);
                $this->totalDurationOfWork[$user->id][$dateIndex] = $this->totalDurationOfWork[$user->id][$dateIndex] ?? 0;

                $startTime = Carbon::parse(date('h:i', (int)$track->touchDate));

                if ($track->type == Tracker::START && !isset($nextTrack)) {
                    $finishTime = Carbon::parse(date('h:i', (int)microtime(true)));
                }

                if ($track->type == Tracker::START && isset($nextTrack) && $nextTrack->type == Tracker::STOP) {
                    $finishTime = Carbon::parse(date('h:i', (int)$nextTrack->touchDate));
                }
                $this->totalDurationOfWork[$user->id][$dateIndex] += $startTime->diffInSeconds($finishTime);
            }
        }

        return $this->totalDurationOfWork;
    }

    private function periodOfMonth(string $filter = null): CarbonPeriod
    {
        $dateFrom = Carbon::now()->startOfMonth()->toDateString();

        $dateTo = Carbon::now()->endOfMonth()->toDateString();

        $filter ? $datePeriod = Carbon::parse($filter)->toPeriod($filter, '1 days') :
            $datePeriod = Carbon::parse($dateFrom)->toPeriod($dateTo, '1 days');

        return $datePeriod;
    }
}
