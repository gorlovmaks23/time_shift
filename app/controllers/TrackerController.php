<?php

declare(strict_types=1);

namespace Timeshift\Controllers;

use Timeshift\Models\Tracker;
use Timeshift\Repository\TrackerRepository;
use Timeshift\Services\LatecomerService;
use Timeshift\Services\SettingService;

final class TrackerController extends ControllerBase
{
    /** @var TrackerRepository $trackerRepository */
    private $trackerRepository;

    /** @var LatecomerService $trackerRepository */
    private $latecomerService;

    /** @var SettingService $settingSerive */
    private $settingSerive;

    public function initialize()
    {
        $this->trackerRepository = $this->di->get(TrackerRepository::class);
        $this->latecomerService = $this->di->get(LatecomerService::class);
        $this->settingSerive = $this->di->get(SettingService::class);
    }

    public function startAction()
    {
        /** @var Tracker $lastTrack */
        $lastTrack = $this->trackerRepository->lastTrackByUserId($this->auth->getIdentity()['id']);

        if (!$lastTrack) {
            $track = new Tracker([
                'userId' => $this->auth->getIdentity()['id'],
                'touchDate' => (int)microtime(true),
                'type' => Tracker::START
            ]);

            $track->save();
        }

        if ($lastTrack && $lastTrack->type == Tracker::STOP) {
            $track = new Tracker([
                'userId' => $this->auth->getIdentity()['id'],
                'touchDate' => (int)microtime(true),
                'type' => Tracker::START
            ]);

            $track->save();
        }

        $this->dispatcher->forward([
            'controller' => 'index',
            'action' => 'index'
        ]);
    }

    public function stopAction()
    {
        /** @var Tracker $lastTrack */
        $lastTrack = $this->trackerRepository->lastTrackByUserId($this->auth->getIdentity()['id']);

        if ($lastTrack && $lastTrack->type == Tracker::START) {
            $track = new Tracker([
                'userId' => $this->auth->getIdentity()['id'],
                'touchDate' => (int)microtime(true),
                'type' => Tracker::STOP
            ]);

            $track->save();
        }

        $this->dispatcher->forward([
            'controller' => 'index',
            'action' => 'index'
        ]);
    }
}
