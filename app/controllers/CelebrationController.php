<?php

declare(strict_types=1);

namespace Timeshift\Controllers;

use Phalcon\Mvc\Controller;
use Timeshift\Repository\CelebrationRepository;

final class CelebrationController extends Controller
{
    /** @var CelebrationRepository $celebrationRepository */
    private $celebrationRepository;

    public function initialize()
    {
        $this->celebrationRepository = $this->di->get(CelebrationRepository::class);
    }

    public function indexAction()
    {
        $celebration = $this->celebrationRepository->celebrationForCurrentDay();

        $this->view->setVar('celebration', $celebration);
    }
}
