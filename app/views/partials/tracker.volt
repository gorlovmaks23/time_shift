<div style="margin-bottom=20px;">
    {% if lastTrack.type is defined and lastTrack.type == startType %}
        {{ link_to('tracker/stop', 'stop', 'class':'btn btn-primary') }}
    {% else %}
        {{ link_to('tracker/start', 'start', 'class':'btn btn-danger') }}
    {% endif %}
</div>
