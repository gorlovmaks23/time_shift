<div class="container">
    <div class="row">
        <div class="col-md-6">
            <p>{{ tracker }}</p>
            <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button"
               aria-expanded="false" aria-controls="multiCollapseExample1">Hide/Show</a>
        </div>
        <div class="col-md-6">
            <h2 class="mb-sm-6 pb-sm-2">Calendar</h2>
            {{ form("index", 'role': 'form') }}

            <div class="form-group row">
                <div class="col-md-9">
                    {{ form.render('date' , ['class' : 'form-control ', 'id' : 'datepicker']) }}
                    <br>
                    {{ submit_button("Search", "class": "btn btn-success") }}
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col">
            <div class="collapse multi-collapse" id="multiCollapseExample1">
                <div class="card card-body">
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th scope="col">DAYS</th>
                            {% for user in users %}
                                <th>{{ user.name }}</th>
                            {% else %}
                            {% endfor %}
                        </tr>
                        </thead>
                        <tbody>

                        {% for period in datePeriod %}
                            <tr>
                                <th scope="row">{{ period.format('d') }} <br> {{ period.isoFormat('dd') }}</th>
                                {% for user in users %}
                                    <th scope="row">
                                        {% for track in user.tracks %}
                                            {% if period.format('Y-m-d') === date('Y-m-d', track.touchDate) %}
                                                {% if (loop.index % 2) > 0 %}
                                                    <span style="color:green">{{ date('h:i',track.touchDate) }}</span>
                                                {% else %}
                                                    <span style="color:#ccc">{{ date('h:i',track.touchDate) }}</span>
                                                    <br>
                                                {% endif %}
                                            {% endif %}
                                        {% endfor %}

                                        {% if(totalDurationOfWork[user.id][period.format('Y-m-d')] is defined) %}
                                        <br><span
                                            style="color:red;">Total: {{ date('h:i', totalDurationOfWork[user.id][period.format('Y-m-d')]) }}</span>
                                        {% endif %}
                                    </th>
                                {% endfor %}
                            </tr>
                        {% else %}
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div style="width:100%">
            <div class="card card-body">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th scope="col">DAYS</th>
                        <th>{{ currentUser.name }}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th scope="row">{{ currentDay.format('d') }} <br> {{ currentDay.isoFormat('dd') }}</th>
                        <th scope="row">
                            {% for track in currentUser.tracks %}
                                {% if currentDay.format('Y-m-d') === date('Y-m-d', track.touchDate) %}
                                    {% if (loop.index % 2) > 0 %}
                                        <span style="color:green">{{ date('h:i', track.touchDate) }}</span> -
                                    {% else %}
                                        <span style="color:#ccc">{{ date('h:i', track.touchDate) }}</span><br>
                                    {% endif %}

                                    {% if (loop.last) %}
                                        <br><span
                                        style="color:red;">Total: {{ date('h:i',totalDurationOfWork[currentUser.id][date('Y-m-d',track.touchDate)]) }}</span>
                                    {% endif %}
                                {% endif %}
                            {% endfor %}
                        </th>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
