<?php

declare(strict_types=1);

namespace Timeshift\Models;

use Phalcon\Mvc\Model;

final class Settings extends Model
{
    /** @var integer */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $value;
}
