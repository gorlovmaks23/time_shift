<?php

declare(strict_types=1);

namespace Timeshift\Models;

use Phalcon\Mvc\Model;

final class Celebrations extends Model
{
    /** @var integer */
    public $id;

    /** @var string */
    public $name;

    /** @var string */
    public $date;

    /** @var string */
    public $repetitive;
}
