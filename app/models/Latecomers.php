<?php

declare(strict_types=1);

namespace Timeshift\Models;

use Phalcon\Mvc\Model;

final class Latecomers extends Model
{
    /** @var integer */
    public $id;

    /** @var string */
    public $date;

    /** @var integer */
    public $userId;
}
