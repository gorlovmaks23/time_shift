<?php

declare(strict_types=1);

namespace Timeshift\Models;

use Phalcon\Mvc\Model;

final class Tracker extends Model
{
    const START = 1;
    const STOP = 2;

    /** @var integer */
    public $id;

    /** @var integer */
    public $userId;

    /** @var integer */
    public $touchDate;

    /** @var integer */
    public $type;

    public function initialize()
    {
        $this->belongsTo('userId', Users::class, 'id', [
            'alias' => 'user',
            'reusable' => true
        ]);
    }
}
